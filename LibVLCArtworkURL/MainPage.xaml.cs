﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using LibVLCSharp.Platforms.Windows;
using LibVLCSharp.Shared;


// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace LibVLCArtworkURL
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        LibVLC libvlc;
        MediaPlayer mp;
        Media media;

        public MainPage()
        {
            this.InitializeComponent();
            VideoView.Initialized += VideoView_Initialized;
        }

        private async void VideoView_Initialized(object sender, InitializedEventArgs e)
        {
          libvlc = new LibVLC(enableDebugLogs: true, e.SwapChainOptions);
          libvlc.Log += LibVLC_Log;
          mp = new MediaPlayer(libvlc);
          var file = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFileAsync("Gymnopedie No 1.mp3");
          media = new Media(libvlc, file.Path, FromType.FromPath);
          media.MetaChanged += Media_MetaChanged;
          //mp.Play(media);
          await media.Parse(MediaParseOptions.FetchLocal);
          Debug.WriteLine("Artwork URL parsed as: " + media.Meta(MetadataType.ArtworkURL));
          media.Dispose();
        }

        private void Media_MetaChanged(object sender, MediaMetaChangedEventArgs e)
        {
          Debug.WriteLine("Meta changed; ArtworkURL is now: " + media.Meta(MetadataType.ArtworkURL));
        }

        private void LibVLC_Log(object sender, LogEventArgs e)
        {
          Debug.WriteLine("[libvlc] " + e.Message);
        }
  }
}
